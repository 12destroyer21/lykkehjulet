#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <string>


using namespace std;

string word;                    // Hemmeligt ord
int attempts;                   // Antal forsøg
string category = "FRUITS";     // Kategori

string words[] = {"Pineapple", "Banana", "Apple", "Orange", "Strawberry"};    // Mulige hemmelige ord

bool guessed[20];                // Status på spillet i et array
int N = 0;                       // Antal bogstaver i ordet der skal gættes

// Vælg et tilfældigt ord som skal gættes.
void random_word(){
    int i = rand()*5./RAND_MAX;
    word = words[i];
    N = word.length();
}

/* Klargør random generator og vælg første ord */
void setup(){
    // Random Seed
    srand(time(NULL));
    rand();

    random_word();
    cout << "The category is: " << category << endl << endl;
}

/* Opdaterer hvor meget der er gættet */
void update() {

    char letter;

    for (int i = 0; i < N; i++) {
        if (guessed[i]) {
          letter = word[i];
        } else {
          letter = '_';
        }
        cout << letter << " ";;
    }
    cout << endl;
}

/* Gæt et bogstav */
void guess() {

  char letter;

  cout << "Guess a single character: ";
  cin >> letter;
  letter = tolower(letter);

  for (int i = 0; i < N; i++) {
    if (letter == tolower(word[i])){
            guessed[i] = true;
    }
  }
}

int main(){
    setup();

    while(true){
        update();
        guess();
    }

    return 0;
}



